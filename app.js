// ----------------------------------------------------------------------------
// Requires
// ----------------------------------------------------------------------------

// DotEnv
require('dotenv').config();

// Modules NodeJS
const fs = require('fs');

// ExpressJS
const express = require('express');
const app = express();

// Serveur HTTP
const http = require('http').createServer(app);

// Module Config qui gère la configuration MAC et les urls serveurs
const Config = require('./modules/config');
const config = new Config();

// Module Sequence gérant les séquences et les slots associés
const Sequence = require('./modules/sequence');
const sequence = new Sequence();

// Module Settings gérant les paramètres du raspberry (rotation, debug, etc.)
const Settings = require('./modules/settings');
const settings = new Settings();

// Module System récupérant les infos systèmes
const System = require('./modules/system');

// Module Network gérant la récupération de la MAC Address
const Network = require('./modules/network');
const network = new Network();

// Module gérant les téléchargements
const DownloadManager = require('./modules/downloadManager');
const downloadManager = new DownloadManager();

// Module Wifi
const Wifi = require('./modules/wifi/wifi');
var wifi = new Wifi();

// OMX Manager
const OmxManager = require('omx-manager');
const manager = new OmxManager();
let cameraInstance;

const log = require('./modules/log');

// Variables globales
let _mac_address = '';
let _localSocket;
let _currentPage = null;
let _serverSocket;
let _currentSpeedBps = 0;
let _commit;
let _systemInfos = {};

// Require child_process
const exec = require('child_process').exec;

// ----------------------------------------------------------------------------
// System
// ----------------------------------------------------------------------------
async function getStats() {
    // CPU
    let cpu = System.getCpuLoad();
    // Mem
    let used_memory = System.getMemory();
    // Mem
    let used_disk_space = System.getFileSystemSize();
    // Network
    let network = System.getNetworkStats();
    // Mem
    let temperature = System.getCpuTemperature();

    return Promise.all([cpu, used_memory, used_disk_space, network, temperature])
        .then((data) => {
            _systemInfos = {
                cpu: Math.round(data[0].currentload * 100) / 100,
                used_memory: data[1].active,
                used_disk_space: data[2][1].used,
                rx_sec: Math.round(data[3].rx_sec * 100) / 100,
                tx_sec: Math.round(data[3].tx_sec * 100) / 100,
                temperature: data[4].main,
                uptime: System.getTime().uptime
            };

            return _systemInfos;
        })
        .catch((err) => {
            return err;
        });
}

function checkUptime() {
    let time = System.getTime();
    if (time.uptime > 86400) { // 1 jour
        shutdown(function(output) {
            log.info("Reboot due by reached max uptime: " + time.uptime);
        });
    }
}

// Shutdown function
function shutdown(callback) {
    exec('reboot', function(error, stdout, stderr) {
        callback(stdout);
    });
}

// Toutes les 30s
setInterval(() => {
    // On récupère les stats ...
    getStats().then((data) => {
    }).catch((err) => {
        log.error(err);
    });
    // On reboot si l'uptime est superieur à XX heures
    checkUptime();
}, 30000); // toutes les 30 secondes

// ----------------------------------------------------------------------------
// Sockets
// ----------------------------------------------------------------------------

const socketClient = require('socket.io-client');
const socketServer = require('socket.io');

// Socket Server
function startRemoteServer() {
    let connectionAttempt = 0;
    // On attend une seconde avant de se connecter au serveur, pour que le SQL soit à jour
    setTimeout(
        function() {
            // Connexion
            log.info("Connexion: " + config.server);

            // Socket client
            const clientServer = socketClient(config.server);

            // Event - Connecté au serveur
            clientServer.on('connect', function() {
                log.success("Connecté");
                log.info("Login ...");

                clientServer.emit('login', {
                    mac_address: _mac_address
                });
            });

            // Event - Reconnecté au serveur
            clientServer.on('reconnect', function() {
                log.debug("Reconnecté");
            });
            // Event - Erreur de connexion au serveur
            clientServer.on('connect_error', function() {
                log.error("Erreur connexion au serveur Node");
                connectionAttempt++;
                // Si on a plus de 10 échecs de connexion au serveur, on déconnecte, on supprime le serveur de la config et on reconnecte au portal
                if (connectionAttempt >= 10) {
                    log.warn("10 échecs de connexion au serveur, déconnexion et requête au portail");
                    clientServer.disconnect();
                    // Connexion au portal
                    startRemotePortal()
                        .then(() => {
                            startRemoteServer();
                        });
                }
            });

            // Event - Erreur
            clientServer.on('error', function(data) {
                log.error("Erreur");
                log.error(data);
            });

            // Event - Logged
            clientServer.on('logged', function() {
                log.success("Loggé");
                _serverSocket = clientServer;

                if (_serverSocket.connected && typeof _commit !== "undefined") {
                    _serverSocket.emit("debug:commit", { commit: _commit });
                }
            });

            // Event - Déconnecté
            clientServer.on('disconnect', function() {
                log.debug("Déconnecté du serveur");
            });

            // Event - Sequence envoyée par le serveur
            clientServer.on('sequence:send', function(data) {
                log.info("Sequence reçue");
                saveSequence(data);
                if (_currentPage != 'sequence') {
                    _localSocket.emit('redirect', 'loading');
                }
            });

            // Event - Reboot du device demandée
            clientServer.on('device:reboot', function() {
                log.info("Reboot en cours ...");

                // Reboot computer
                shutdown(function(output) {
                    log.info("Reboot requested");
                });
            });

            // Event - Reboot du device demandée
            clientServer.on('device:reset', function() {
                log.info("Reset en cours ...");

                // - écran de loading
                _localSocket.emit('redirect', 'loading');

                // - On arrête la vidéo s'il y en a une en cours
                try {
                    cameraInstance.stop();
                } catch (err) {
                    log.warn("Video not playing");
                }

                // Commandes de reset
                let cmdCd = `cd ${__dirname}`;
                let cmdRmConfig = `rm -f data/config.json`;
                let cmdRmRotation = `rm -f data/rotation`;
                let cmdRmSettings = `rm -f data/settings.json`;
                let cmdRmSequence = `rm -f data/sequence.json`;
                let cmdRmAssets = `rm -rf assets/*`;
                let cmdRmWpa = `rm -f /etc/wpa_supplicant/wpa_supplicant-wlan0.conf`;
                let cmdCpConfig = `cp data/samples/config.json data/`;
                let cmdCpSettings = `cp data/samples/settings.json data/`;
                let cmdCpWpa = `cp data/samples/wpa_supplicant-wlan0.conf /etc/wpa_supplicant`;
                let cmdRmDhcp = `rm -f /var/lib/dhcpd/*.lease`;
                let cmdRmDnsmasq = `rm -f /var/lib/misc/dnsmasq.leases`;

                // - reset
                exec(
                    `${cmdCd} && ${cmdRmConfig} && ${cmdRmRotation} && ${cmdRmSettings} && ${cmdRmSequence} && ${cmdRmAssets} && ${cmdRmWpa} && ${cmdCpConfig} && ${cmdCpSettings} && ${cmdCpWpa} && ${cmdRmDhcp} && ${cmdRmDnsmasq}`, { shell: '/bin/bash' },
                    (errReset) => {
                        if (!errReset) {
                            // - emit
                            log.info('Reset effectué');
                            clientServer.emit('device:reseted');
                        } else {
                            log.err('Reset échoué');
                            log.err(errReset);
                            clientServer.emit('reset:failed');
                        }
                    }
                );

            });

            // Event - Settings envoyées par le serveur
            clientServer.on('settings:send', function(data) {
                log.info("Settings reçues");
                saveSettings(data);

                // set the timezone of the box
                exec('timedatectl set-timezone ' + data.timezone);
            });

            // On envoie les infos systèmes au serveur toutes les minutes
            setInterval(() => {
                clientServer.emit('device:stats', _systemInfos);
            }, 60000);
        },
        1000
    );
}

// Socket Portal
function startRemotePortal() {
    return new Promise((resolve, reject) => {
        // Connexion au portail
        log.info("Connexion au portail: " + config.portal);

        let clientPortal = socketClient(config.portal);

        // Connexion établie
        clientPortal.on('connect', function() {
            log.success("Connecté au portal");
            log.info("Envoi de la Mac Address");
            log.debug(_mac_address);

            clientPortal.emit('mac_address:send', {
                mac_address: _mac_address
            });

            clientPortal.on('server:send', (server) => {
                config.save({ server: server })
                    .then(() => {
                        // Server enregistré, on valide
                        log.success(`Serveur reçu: ${server}`);
                        resolve();
                    })
                    .catch((err) => {
                        // échec d'enregistrement, on rejecte et on kill le socket
                        reject(err);
                    });
            });
        });

        // TODO
        // Timeout de X secondes qui enclenche le catch ?
    });
}

// Socket angular local
function startLocalSocket() {
    // On retourne la promise une fois la mac addresse lue
    return new Promise((resolve, reject) => {
        // Socket client
        const localServer = socketServer(http);

        localServer.on('connection', function(localSocket) {
            log.success("WEBAPP: Connected.");

            _localSocket = localSocket;
            // On envoie les infos à angular
            log.info("PLATFORM: " + config.platform);
            _localSocket.emit('platform', config.platform);
            _localSocket.emit('mac', _mac_address);
            _localSocket.emit('sequence', sequence);
            _localSocket.emit('settings', settings);

            // Lancement d'une vidéo OMX - fichier
            _localSocket.on('video:play', function(filename) {
                // on teste si la vidéo est bien présente sur le disque
                fs.stat(`${__dirname}/assets/${filename}`, function(err, stats) {
                    // TODO: Utiliser stats pour définir/envoyer la taille du fichier à angular et comparer avec l'asset
                    if (!err) {
                        log.debug("Lecture video (via OMX): " + filename);
                        try { 
                            cameraInstance.stop();
                            log.info("Vidéo stoppée dans le try");
                        } catch (err) {
                            log.error("Aucune vidéo stoppée");
                        };
                        if (settings.debug == 1) {
                            cameraInstance = manager.create(`${__dirname}/assets/${filename}`, { '--orientation': settings.rotation, '--vol': 0, '--win': '0 0 320 240' });
                        } else {
                            cameraInstance = manager.create(`${__dirname}/assets/${filename}`, { '--orientation': settings.rotation, '--vol': 0,  });
                        }
                        cameraInstance.on('end', () => {
                            _localSocket.emit('video:ended', true);
                        });
                        cameraInstance.play();
                    } else {
                        log.error("Fichier non trouvé: " + filename);
                        // delai avant de passer prévenir angular
                        setTimeout(() => {
                            // TODO a remplacer par un autre emit event
                            _localSocket.emit('video:ended', true);
                        }, 2000);
                    }
                });
            });

            _localSocket.on('audio:play', function(filename) {
                // on teste si la vidéo est bien présente sur le disque
                fs.stat(`${__dirname}/assets/${filename}`, function(err, stats) {
                    // TODO: Utiliser stats pour définir/envoyer la taille du fichier à angular et comparer avec l'asset
                    if (!err) {
                        log.debug("Lecture audio (via OMX): " + filename);
                        try {
                            cameraInstance.stop();
                            log.info("audio stoppé dans le try");
                        } catch (err) {
                            log.error("Aucun audio stoppé");
                        };
                        if (settings.debug == 1) {
                            cameraInstance = manager.create(`${__dirname}/assets/${filename}`, { '--orientation': settings.rotation, '-o': 'both', '--vol': 0, '--win': '0 0 320 240' });
                        } else {
                            cameraInstance = manager.create(`${__dirname}/assets/${filename}`, { '--orientation': settings.rotation, '-o': 'both', '--vol': 0 });
                        }
                        cameraInstance.on('end', () => {
                            _localSocket.emit('audio:ended', true);
                        });
                        cameraInstance.play();
                    } else {
                        log.error("Fichier non trouvé: " + filename);
                        // delai avant de passer prévenir angular
                        setTimeout(() => {
                            // TODO a remplacer par un autre emit event
                            _localSocket.emit('audio:ended', true);
                        }, 2000);
                    }
                });
            });

            // Lancement d'une vidéo OMX - stream
            _localSocket.on('stream:play', function(url) {
                // For now, invalid URLs are just ignored and the next slot is played - if necessary, error checking can be done with rtmpdump
                log.info ("starting to play stream");
                if (settings.debug == 1) {
                    cameraInstance = manager.create(url, { '--orientation': settings.rotation, '--win': '0 0 320 240' });
                } else {
                    cameraInstance = manager.create(url, { '--orientation': settings.rotation });
                }
                cameraInstance.on('end', () => {
                    log.info("stream ended");
                    _localSocket.emit('stream:ended', true);
                });
                cameraInstance.on('error', () => {  // TODO: test if this actually does something/this event is actually emitted
                    log.info("ERROR streaming file");
                    _localSocket.emit('stream:ended', true);
                });
                cameraInstance.play();
            });

            // Event de déconnexion de la webapp
            _localSocket.on('disconnect', function() {
                log.warn("WEBAPP: Disconnected.");
            });

            // Event de déconnexion de la webapp
            _localSocket.on('page:change', function(page) {
                _currentPage = page;
            });

            setInterval(() => {
                if (settings._debug) {
                    _localSocket.emit('system:infos', _systemInfos);
                }
            }, 30000);

            // On valide la promise
            resolve();
        });

        // TODO
        // Timeout de X secondes qui enclenche le catch ?
    });
}

// ----------------------------------------------------------------------------
// Downloader
// ----------------------------------------------------------------------------
function progress(state) {
    //Calcul de la vitesse de téléchargements à afficher (en b, Kb ou Mb /s)
    let speed;
    if (state.speed > 1024) {
        if (state.speed > 1048576) {
            speed = `${(state.speed/1048576).toFixed(2)}Mb/s`;
        } else {
            speed = `${(state.speed/1024).toFixed(2)}Kb/s`;
        }
    } else {
        speed = state.speed + "b/s";
    }
    state.formattedSpeed = speed;
    // Envoi de l'event "state download" au serveur
    if (_serverSocket.connected && state.speed != _currentSpeedBps) {
        _currentSpeedBps = state.speed;
        _serverSocket.emit("download:state", { speed: _currentSpeedBps });
    }
    // Envoi des informations à angular
    _localSocket.emit('progress', state);
}

function downloadAssets() {
    console.log("Starting asset download");
    sequence.getAssets().then((assets) => {
        if (assets.length <= 0) {
            log.info("Aucun fichier à télécharger!");
            if (sequence.slots) {
                log.debug('Envoi des slots');
                _localSocket.emit('slots:send', sequence.slots);
                _localSocket.emit('redirect', 'sequence');
            } else {
                _localSocket.emit('redirect', 'empty');
            }
            // Envoi de l'event "downloaded" au serveur
            if (_serverSocket.connected) {
                _serverSocket.emit("assets:downloaded", true);
            }
        } else {
            // Envoie du nombre de fichiers à télécharger à angular
            // Téléchargements des fichiers
            downloadManager.download(assets, 'assets/', progress, "https://inovhub.s3.eu-west-3.amazonaws.com/speed.test" /*sequence.speedFile*/ )
                .then(function() {
                    log.success("Fichier(s) téléchargé(s)");
                    // Envoi de l'event "downloaded" au serveur
                    if (_serverSocket.connected) {
                        _serverSocket.emit("assets:downloaded", true);
                    }
                    // Changement de page angular
                    if (sequence.slots) {
                        _localSocket.emit('slots:send', sequence.slots);
                        _localSocket.emit('redirect', 'sequence');
                    } else {
                        _localSocket.emit('redirect', 'empty');
                    }
                })
                .catch(function(err) {
                    log.error(err);
                    log.error("Impossible de télécharger le(s) fichier(s)");
                });
        }
    });
}

// ----------------------------------------------------------------------------
// Serveur web
// ----------------------------------------------------------------------------
function startServer() {
    // On définit les assets statiques (CSS, JS, etc.)
    app.use('/libs', express.static('node_modules'));
    app.use('/assets', express.static('assets'));
    app.use(express.static('www'));

    // On définit une route par défaut
    app.get('/', function(req, res) {
        res.sendFile(__dirname + '/www/index.html');
    });

    // On ouvre le serveur sur le port 3000
    http.listen(3000, function() {
        log.info('Listening on *:3000');

        // Un fois le serveur lancé, on lance le localSocket
        startLocalSocket()
            .then(() => {
                loadDebug();
                readConfig();
            });
    });
}

// ----------------------------------------------------------------------------
// Logique
// ----------------------------------------------------------------------------

// 0. Initialisation
log.info("Initialisation ------------------------------------------------");

// 1. On lance le serveur
startServer();

// Lit la configuration sauvegardée
function readConfig() {
    config.read()
        .then(function() {
            log.success("LOCAL: Configuration trouvée");
            //log.debug(data);
        })
        .catch(function() {
            // Configuration non trouvée
            log.warn("LOCAL: Configuration non trouvée");
        })
        .then(function(data) {
            loadMacAddress()
                .then((mac) => {
                    // On charge la mac address
                    _mac_address = mac;
                    log.info(`Mac address loaded: ${_mac_address}`);
                    _localSocket.emit('mac', _mac_address);
                })
                .catch((err) => {
                    log.error(err);
                })
                .then(() => {
                    readSettings()
                        .then((message) => {
                            _localSocket.emit('settings', settings);
                            log.success(message);
                        })
                        .catch((err) => {
                            log.error(err);
                        })
                        .then(() => {
                            let seq;
                            readSequence()
                                .then((dataSequence) => {
                                    seq = dataSequence;
                                    if (dataSequence.slots) {
                                        _localSocket.emit('slots:send', sequence.slots);
                                        _localSocket.emit("redirect", "sequence");
                                    } else {
                                        _localSocket.emit("redirect", "empty");
                                    }
                                })
                                .catch((err) => {
                                    log.error(err);
                                })
                                .then(() => {
                                    if (_mac_address != '') {
                                        if (!seq) {
                                            // Si pas de séquence, on envoie sur register
                                            _localSocket.emit("redirect", "loading");
                                        }
                                        // Check de la connexion internet
                                        manageInternet()
                                            .then(() => {
                                                // Wifi hotspot activé = pas de co
                                                if (wifi.enabled) {
                                                    // Wifi connected
                                                    if (!seq) {
                                                        _localSocket.emit("redirect", "wifi");
                                                    }
                                                    wifi.on('wifi:connected', (res) => {
                                                        log.info("Wifi connected", res);
                                                        if (!seq) {
                                                            // Si pas de séquence, on envoie sur register
                                                            _localSocket.emit("redirect", "register");
                                                        } else if (_currentPage != "sequence") {
                                                            _localSocket.emit("redirect", "sequence");
                                                        }
                                                        // On lance les socket portal ou server
                                                        if (config.server == '') {
                                                            // Connexion au portal
                                                            startRemotePortal()
                                                                .then(() => {
                                                                    startRemoteServer();
                                                                });

                                                        } else {
                                                            // Connexion au serveur
                                                            startRemoteServer();
                                                        }
                                                    });
                                                } else {
                                                    if (!seq) {
                                                        // Si pas de séquence, on envoie sur register
                                                        _localSocket.emit("redirect", "register");
                                                    }
                                                    // Pas de wifi enabled = connexion établie
                                                    // On lance les socket portal ou server
                                                    if (config.server == '') {
                                                        // Connexion au portal
                                                        startRemotePortal()
                                                            .then(() => {
                                                                startRemoteServer();
                                                            });

                                                    } else {
                                                        // Connexion au serveur
                                                        startRemoteServer();
                                                    }
                                                }
                                            })
                                            .catch((err) => {
                                                log.error(err);
                                            });
                                    } else {
                                        _localSocket.emit("redirect", "error");
                                    }
                                });;
                        });
                });
        });
}

// Lit les paramètres sauvegardés du device
function readSettings() {
    return new Promise((resolve, reject) => {
        settings.read()
            .then(function(data) {
                resolve("LOCAL: Settings trouvés");
                //log.debug(data);
            })
            .catch(function() {
                reject("LOCAL: Settings non trouvés");
            });
    });
}

// Sauvegarde les settings
function saveSettings(data) {
    settings.save(data)
        .then(function() {
            log.success("LOCAL: Settings sauvegardées");
            _localSocket.emit('settings', settings);
        })
        .catch(function() {
            log.error("LOCAL: Impossible de sauvegarder les settings");
        });
}

// Lit la séquence sauvegardée
function readSequence() {
    return new Promise((resolve, reject) => {
        sequence.read()
            .then((data) => {
                resolve(data);
            })
            .catch(() => {
                reject("Séquence non trouvée");
            });
    });
}

// Sauvegarde la séquence
function saveSequence(data) {
    sequence.save(data)
        .then(function() {
            log.success("LOCAL: Séquence sauvegardée");
            downloadAssets();
        })
        .catch(function(err) {
            log.error("LOCAL: Impossible de sauvegarder la séquence");
            console.log(err);
        });
}

// Charge la Mac address en global
function loadMacAddress() {
    return new Promise((resolve, reject) => {
        network.getMacAddress().then(function(mac) {
            resolve(mac);
        }).catch(function() {
            reject("Impossible de récupérer la MAC ADDRESS");
        });
    });
}

// Checke la connexion internet et lance le wifi si non connecté
function manageInternet() {
    return new Promise((resolve, reject) => {
        log.debug("Vérification connexion internet");
        network.checkConnection()
            .then(() => {
                resolve();
                log.success('Connecté à Internet');
            })
            .catch((errTest) => {
                log.error('Non connecté à Internet');
                // On désactive le wifi avant de la réactiver
                wifi.disable()
                    .then(() => {
                        log.info('Hotspot disabled');
                        wifi.enable()
                            .then((wifiData) => {
                                settings.save(wifiData)
                                    .then(() => {
                                        log.info(`Wifi Data ${wifiData.ssid}/${wifiData.passphrase}`);
                                        _localSocket.emit('settings', settings);
                                    })
                                    .catch((err) => {
                                        log.error(err);
                                    })
                                    .then(() => {
                                        resolve();
                                    });
                            })
                            .catch((err) => {
                                log.error(err);
                                reject(err);
                            });
                    })
                    .catch((err) => {
                        log.error(err);
                    });
            });
    });
}

// Récupère le hash du dernier commit
function lastCommit() {
    return new Promise((resolve, reject) => {
        require('child_process').exec('git rev-parse HEAD', function(errHash, stdoutHash) {
            if (!errHash) {
                let hash = stdoutHash.trim();
                require('child_process').exec('git log -n1 --format="%at"', function(errDate, stdoutDate) {
                    if (!errDate) {
                        let date = stdoutDate.trim();
                        resolve({ hash: hash, date: date });
                    } else {
                        reject(errDate);
                    }
                });
            } else {
                reject(errHash);
            }
        });
    });
}

function loadDebug() {
    lastCommit()
        .then((commit) => {
            _commit = commit;
            _localSocket.emit('debug', { 'commit': commit });
        })
        .catch((err) => {
            log.error(err);
        });
}
