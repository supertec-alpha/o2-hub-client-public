'use strict';

const fs = require('fs');

class Command {

    constructor() {
        this._path = "./commands/";
        this._commands = [
            {
                execution: function() {
                    return new Promise((resolve, reject) => {
                        let file = "./config.txt";
                        fs.readFile(file, 'utf8', function(err, data) {
                            if (!err) {
                                if (data.length > 0) {
                                    const regex = /(avoid_warnings)=(\d)/g;
                                    let foundData, m;
                                    let foundRegex = false;
                                    while ((m = regex.exec(data)) !== null) {
                                        foundRegex = true;
                                    }
                                    if(foundRegex) {
                                        data = data.replace(regex, "$1=1");
                                    }
                                    else {
                                        data += "\navoid_warnings=1";
                                    }
                                    fs.writeFile(file, data, 'utf8', function(err) {
                                        if (err) reject(err);
                                        resolve();
                                    });
                                } else {
                                    reject();
                                }
                            } else {
                                reject(err);
                            }
                        });
                        // Ouverture du fichier /boot/config.txt
                        // Recherche de "avoid_warnings"
                        // mise à 1 s'il existe
                        // écriture s'il n'exsite pas
                    });
                },
                file: "indicators"
            }
        ];
        this.init();
    }

    init() {
        let that = this;

        // Pour chaque commandes, on vérifie si le fichier existe
        // Si non, on execute la commande
        var pCommands = [];
        that._commands.forEach((command) => {
            pCommands.push(new Promise((resolve, reject) => {
                fs.stat(that._path+command.file, function(err, stats) {
                    if (err || typeof stats === "undefined") {
                        command.execution()
                            .then(() => {
                                fs.appendFile(that._path+command.file, '', (err) => {
                                    resolve();
                                });
                            })
                            .catch((err) => {
                                console.log(err);
                                reject(err);
                            });
                    }
                    else {
                        resolve();
                    }
                });
            }));
        });
        Promise.all(pCommands)
            .then(() => {
                console.log("Commandes exécutées");
            })
            .catch(() => {
                console.log("Erreur lors de l'exécution des commandes");
            });

    }

}

module.exports = Command;