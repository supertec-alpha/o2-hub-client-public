'use strict';

const os = require('os');
const macaddress = require('macaddress');
const dns = require('dns');
const { exec, execSync } = require('child_process');

class Network {

    getMacAddress() {
        return new Promise(function(resolve, reject) {
            if (os.platform() === 'win32') {
                macaddress.one(function(err, mac) {
                    if (err) reject(err);
                    resolve(mac);
                });
            } else {
                exec('cat /sys/class/net/eth0/address', function(err, stdout, stderr) {
                    if (err) reject(err);
                    resolve(stdout.toUpperCase().trim());
                });
            }
        });
    }

    checkConnection() {
        return new Promise((resolve, reject) => {
            let loop = 0;
            let interval = setInterval(() => {
                dns.resolve('www.google.com', (errTest) => {
                    if (errTest == null) {
                        clearInterval(interval);
                        resolve({ connection: true, error: null });
                    }
                    if (loop >= 3) {
                        clearInterval(interval);
                        reject({ connection: false, errTest });
                    }
                    loop++;
                });
            }, 3000);
        });
    }

}

module.exports = Network;