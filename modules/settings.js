'use strict';

const file = require('./file');
const log = require('./log');

class Settings {

    constructor() {
        this._rotation = 270;
        this._debug = 0;
        this._ssid = '';
        this._passphrase = '';
    }

    read() {
        let that = this;
        return file.readJSON('./data/settings.json').then(function(data) {
            that._rotation = data.rotation;
            that._debug = data.debug;
            that._ssid = data.ssid;
            that._passphrase = data.passphrase;
            return data;
        });
    }

    save(data) {
        return new Promise((resolve) => {
            let that = this;
            that._rotation = (typeof data.rotation !== "undefined" && Number.isInteger(data.rotation)) ? data.rotation : that._rotation;
            that._debug = typeof data.debug !== "undefined" ? data.debug : that._debug;
            that._ssid = data.ssid || that._ssid;
            that._passphrase = data.passphrase || that._passphrase;
            that._timezone = typeof data.timezone !== "undefined" ? data.timezone : "GMT"
            file.saveFile('./data/rotation', that._rotation)
                .then(() => {
                    log.info("Fichier Rotation sauvegardé");
                })
                .catch((err) => {
                    log.error(err);
                })
                .then(() => {
                    resolve(file.saveJSON('./data/settings.json', { "rotation": that._rotation, "debug": that._debug, "timezone": that._timezone ,"ssid": that._ssid, "passphrase": that._passphrase }));
                });
        });
    }

    get rotation() {
        return this._rotation;
    }

    set rotation(value) {
        this._rotation = parseInt(value);
    }

    get debug() {
        return this._debug;
    }

    set debug(value) {
        this._debug = parseInt(value);
    }

    get ssid() {
        return this._ssid;
    }

    set ssid(value) {
        this._ssid = parseInt(value);
    }

    get passphrase() {
        return this._passphrase;
    }

    set passphrase(value) {
        this._passphrase = parseInt(value);
    }

}

module.exports = Settings;