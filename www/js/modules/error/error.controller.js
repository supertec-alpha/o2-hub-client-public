/*global angular*/

(function () {
    'use strict';

    angular
        .module('app.error')
        .controller('ErrorController', Error)

    Error.$inject = ['$scope', 'CoreService'];

    function Error($scope, CoreService) {
        //var vm = this;

        activate();

        $scope.mac = CoreService.getMac();
        $scope.$watch(function() {
                return CoreService.getSettings();
            },
            function(sett) {
                $scope.settings = sett;
            }, true
        );

        function activate() {}
    }
})();