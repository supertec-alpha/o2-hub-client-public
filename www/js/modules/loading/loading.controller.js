/*global angular*/

(function() {
    'use strict';

    angular
        .module('app.loading')
        .controller('LoadingController', Loading)

    Loading.$inject = ['$scope', 'CoreService'];

    function Loading($scope, CoreService) {
        //var vm = this;

        activate();

        $scope.mac = CoreService.getMac();
        $scope.$watch(function() {
                return CoreService.getSettings();
            },
            function(sett) {
                $scope.settings = sett;
            }, true
        );

        function activate() {}
    }
})();