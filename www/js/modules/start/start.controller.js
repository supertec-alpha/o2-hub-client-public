/*global angular*/

(function () {
    'use strict';

    angular
        .module('app.start')
        .controller('StartController', Start)

    Start.$inject = ['$scope', 'CoreService'];

    function Start($scope, CoreService) {
        //var vm = this;

        activate();

        $scope.mac = CoreService.getMac();
        $scope.$watch(function() {
                return CoreService.getSettings();
            },
            function(sett) {
                $scope.settings = sett;
            }, true
        );

        function activate() {}
    }
})();