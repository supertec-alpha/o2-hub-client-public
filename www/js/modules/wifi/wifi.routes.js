(function () {
    'use strict';

    angular
        .module('app.wifi')
        .config(configRoutes);

    configRoutes.$inject = ['$stateProvider'];

    function configRoutes($stateProvider) {
        $stateProvider
            .state('wifi', {
                url: '/wifi',
                templateUrl: 'js/modules/wifi/wifi.tmpl.html',
                controller: 'WifiController',
                controllerAs: 'vm'
            });
    }

})();